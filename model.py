import pandas as pd
import numpy as np
from collections import defaultdict
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

from dataset import load_dataset, data_generator, add_features

def convex(x, y, alpha):
    return x * alpha + y * (1 - alpha)


class LinearModel:

    """A simplified linear regressor implementation"""

    def __init__(self, X, Y):

        X = self._preprocess(X)
        Y = np.expand_dims(Y, axis=1)

        self.model = LinearRegression()
        self.model.fit(X, Y)
        
    def _preprocess(self, X):
        X = np.array(X)
        if len(X.shape) == 1:
            X = np.expand_dims(X, axis=1)
        return X
        
    def predict(self, X):
        X = self._preprocess(X)
        return self.model.predict(X).ravel()
    
    @property
    def params(self):
        return self.model.coef_

class Model:

    def __init__(self, alpha=1):
        pass

    def train(self, dataset):
        pass

    def predict(self, data, tmr=None):
        return 0

class MeanModel(Model):

    def __init__(self, alpha=1):
        pass

    def train(self, dataset):
        self.mean = dataset['Volume'].mean()

    def predict(self, *_):
        return self.mean


class MeanRatioModel(Model):

    def __init__(self, alpha=1):
        pass

    def train(self, dataset):
        self.mean = dataset['Volume'].mean()

    def predict(self, *_):
        return 1

class LastValueModel(Model):

    def __init__(self, alpha=1):
        pass

    def predict(self, data, tmr=None):
        return data.iloc[-1].Volume

class SimpleExtrapolationModel(Model):

    def __init__(self, alpha=1):
        pass

    def predict(self, data, tmr=None):
        x1 = data.iloc[-2].Volume
        x2 = data.iloc[-1].Volume
        y = 2 * x2 - x1
        return y

class WeightedLinearRegressionModel(Model):

    def __init__(self, alpha=1, window=100):
        self.window = window
        self.alpha=alpha

    def predict(self, data, tmr=None):

        Y = data.iloc[-self.window:]['Volume'].to_numpy()
        N = len(Y)
        X = np.arange(N)
        W = np.exp((X - N) / N)

        X = np.expand_dims(X, axis=1)
        Y = np.expand_dims(Y, axis=1)

        model = LinearRegression()
        model.fit(X, Y, sample_weight=W)
        pred = model.predict(X)
        today_vol = float(data.iloc[-1]['Volume'])
        tmr_vol = float(model.predict([[N]]))

        y = tmr_vol / today_vol
        return convex(y, 1, self.alpha)


class EventsRatioModel(Model):
    '''
    Returns the volume ratio based on events
    '''

    def __init__(self, alpha=0):
        self.alpha = alpha

    def train(self, dataset):
        X = dataset[['events', 'Volume']].to_numpy()
        self.mean = dataset['Volume'].mean()
        self.means = dataset.groupby('events').mean()['Volume']
        self.model = defaultdict(lambda: self.mean, self.means)

    def predict(self, data, tmr=None):
        mean_today = self.model[data.iloc[-1]['events']]
        mean_tmr = self.model[tmr['events']]
        return convex(mean_tmr / mean_today, 1, self.alpha)

class YearRatioModel(Model):

    '''
    Returns the volume ratio based on the year day
    '''

    def __init__(self, alpha=.1):
        self.alpha = alpha

    def train(self, dataset):
        means = dataset.groupby('yearday')['Volume'].mean()
        means = means.reindex(range(367)).interpolate()
        means = means.ffill().bfill().to_numpy()
        # means = self.smooth(means)
        self.model = means

    def predict(self, data, tmr=None):
        volume = data.iloc[-1]['Volume']
        day = int(data.iloc[-1]['yearday'])
        mean_volume = self.model[day]
        return convex(mean_volume / volume, 1, self.alpha)

    def smooth(self, x, window=7):
        """Cyclic convolution"""
        extended = np.concatenate([x[-window:], x, x[:window]])
        y = np.convolve(extended, np.ones(window) / window, mode='same')
        return y[window:-window]

class DayRatioModel(Model):

    '''
    Returns the volume ratio based on the week day
    '''

    def __init__(self):
        pass

    def train(self, dataset):
        self.mean = dataset['Volume'].mean()
        self.model = dataset.groupby('day').mean()['Volume']

    def predict(self, data, tmr):
        tmr_mean = self.model[tmr['day']]
        today_mean = self.model[data.iloc[-1]['day']]
        return tmr_mean / today_mean

class MonthRatioModel(Model):

    '''
    Returns the volume ratio based on the month
    '''

    def __init__(self):
        pass

    def train(self, dataset):
        self.mean = dataset['Volume'].mean()
        self.model = dataset.groupby('month').mean()['Volume']

    def predict(self, data, tmr):
        today_mean = self.model[data.iloc[-1]['month']]
        tmr_mean = self.model[tmr['month']]
        return tmr_mean / today_mean

class BoostModel(Model):

    '''
    Combine all the ratio models and one absolute value model.
    '''

    def __init__(self, abs_model, *relative_models):
        self.abs_model = abs_model
        self.relative_models = relative_models

    def train(self, data):
        self.abs_model.train(data)
        [m.train(data) for m in self.relative_models]

    def predict(self, data, tmr):

        prediction = self.abs_model.predict(data, tmr)
        for model in self.relative_models:
            prediction *= model.predict(data, tmr)

        return prediction


def evaluate_model(model, dataset):

    """Evaluate the model on the dataset and return MSE and R2 metrics"""

    pred, gt = [], []
    for i, (X, Y) in enumerate(dataset):
        y = model.predict(*X)

        pred.append(y)
        gt.append(Y['Volume'])

    pred, gt = np.array(pred), np.array(gt)
    mse = np.mean((pred - gt) ** 2)
    y_var = np.var(gt)
    e_var = np.var(pred - gt)
    r2 = 1 - e_var / y_var
    return mse, r2

def optimize(model, model2optimize):
    """Evaluate alphas for a certain model in the boosted model"""

    data_train = add_features(load_dataset('train.csv'))
    for alpha in np.arange(0, 1, .02):
        model2optimize.alpha = alpha
        model.train(data_train)
        mse, r2 = cross_validation(model)
        print(alpha, mse, r2)


def cross_validation(model):
    """Evaluate the model by the cross-validation"""

    kf = KFold(n_splits=5)
    dataset = add_features(load_dataset('train.csv')) 
    mses, r2s = [], []
    for train_index, valid_index in kf.split(dataset):
        train_dataset = dataset.iloc[train_index]
        valid_dataset = dataset.iloc[valid_index]
        dataset_train = data_generator(train_dataset, 'train')
        dataset_valid = data_generator(valid_dataset, 'train')
        model.train(train_dataset)
        mse, r2 = evaluate_model(model, dataset_valid)
        mses.append(mse)
        r2s.append(r2)

    return np.mean(mses), np.mean(r2s)


if __name__ == '__main__':

    data_train = add_features(load_dataset('train.csv'))
    data_all = add_features(load_dataset('all.csv'))
    dataset_train = data_generator(data_all, 'train')
    dataset_test = data_generator(data_all, 'test')

    lvm = LastValueModel()
    ratio_models = []
    ratio_models.append(DayRatioModel())
    ratio_models.append(YearRatioModel(.1))
    ratio_models.append(EventsRatioModel(.03))
    ratio_models.append(WeightedLinearRegressionModel(alpha=.4, window=10))
    ratio_models.append(WeightedLinearRegressionModel(alpha=.3, window=50))

    model = BoostModel(lvm, *ratio_models)
    model.train(data_train)

    # model = MeanRatioModel()
    mse, r2 = evaluate_model(model, dataset_train)
    print(f'Train MSE {mse:.3f}, R2: {r2:.3f}')
    mse, r2 = evaluate_model(model, dataset_test)
    print(f'Test MSE {mse:.3f}, R2: {r2:.3f}')

