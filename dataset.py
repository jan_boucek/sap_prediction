import pandas as pd
import datetime
from collections import defaultdict

'''

  |               -|
  |  - shadow_up   |
  |                |
 | |   -,          |
 | |    |          |
 | |    |          |
 | |    | - body   | - size
 | |    |          |
 | |    |          |
 | |   _|          |
  |                |
  |  - shadow_down |
  |               _|

'''

def append(dataset, name, series):
    dataset.insert(dataset.shape[1], name, series)

def read_date(text):
    """ Read the text in the ISO format yyyy-mm-dd"""
    return datetime.datetime.fromisoformat(text)

def load_dataset(path='all.csv'):
    """Load the S&P 500 index data from a csv file"""
    with open(path, 'r') as f:
        data = pd.read_csv(f, index_col='Date')
        data['Volume'] /= 1e9
        data = data.drop('Adj Close', axis=1)
    return data

def add_features(dataset):
    """Add easily computed features to the dataframe"""

    # Compute features
    body_up = dataset[['Open', 'Close']].max(axis=1)
    body_down = dataset[['Open', 'Close']].min(axis=1)
    body = body_up - body_down
    shadow_down = body_down - dataset['Low']
    shadow_up = dataset['High'] - body_up
    range_ = (dataset['High'] - dataset['Low'])
    body_ratio = body / range_
    vol_diff = -dataset['Volume'].diff(-1)
    vol_diff_r = -dataset['Volume'].diff(-1) / dataset['Volume']
    gt = dataset['Volume'].shift(-1)

    # Add the features
    append(dataset, 'body', body)
    append(dataset, 'shd_u', shadow_up)
    append(dataset, 'shd_d', shadow_down)
    append(dataset, 'range', range_)
    append(dataset, 'body_r', body_ratio)
    append(dataset, 'diff', vol_diff)
    append(dataset, 'diff_r', vol_diff_r)
    append(dataset, 'gt', gt)

    # Add other features
    dataset = add_events(dataset)
    dataset = add_days(dataset)
    dataset = add_month(dataset)
    dataset = add_year_day(dataset)

    return dataset.iloc[:-1]

def add_days(dataset):
    """Add the week day number and onehot representation to a dataframe""" 
    day = lambda x: int(read_date(x).weekday())
    days = dataset.index.to_frame()['Date'].map(day)
    days_onehot = pd.get_dummies(days, prefix='d')
    append(dataset, 'day', days)
    dataset = pd.concat([dataset, days_onehot], axis=1)
    return dataset

def add_month(dataset):
    """Add the month number to a dataframe""" 
    month = lambda x: int(read_date(x).month)
    months = dataset.index.to_frame()['Date'].map(month)
    append(dataset, 'month', months)
    return dataset

def add_year_day(dataset):
    """Add the year day to a dataframe""" 
    year = lambda x: int(read_date(x).timetuple().tm_yday)
    yeardays = dataset.index.to_frame()['Date'].map(year)
    append(dataset, 'yearday', yeardays)
    return dataset

def add_events(dataset, path='events.txt'):
    """Load and add the number of events for each day in the dataframe"""

    with open(path, 'r') as f:
        lines = [e.strip() for e in f.readlines()]
    
    events = defaultdict(int)
    for line in lines:
        try:
            date = datetime.datetime.strptime(line, "%A, %B %d, %Y")
            date = date.strftime('%Y-%m-%d')
        except ValueError:
            events[date] += 1

    events = {d : events[d] for d in dataset.index}
    df = pd.DataFrame.from_dict(events, orient='index', columns=['event'])
    append(dataset, 'events', df)
    return dataset

def data_generator(dataset, subset='train', min_length=10):
    """Create the data generator for evaluation from dataframe"""

    if subset == 'train':
        data = dataset.iloc[min_length:].loc[:'2017']
    elif subset == 'test':
        data = dataset.loc['2017':]

    to_know = ['events', *[f'd_{i}' for i in range(5)]]
    to_know.extend(['day', 'month'])
    for date, day in data.iterrows():
        X = dataset.loc[:date].iloc[:-1], day[to_know]
        Y = day
        yield X, Y

